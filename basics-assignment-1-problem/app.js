const app = Vue.createApp({
    data(){
        return {
            name: 'Please Enter Your Name',
            age: 44,
            imageLink : 'https://company-website-wp-s3-bucket.s3.ap-southeast-1.amazonaws.com/wp-content/uploads/2020/08/06173849/Google-Workspace-3-1024x576.jpg'
        };
    },
    methods:{
        calcAge(){
            return this.age + 5
        },
        randomize(){
            return Math.random()
        }
    }
});

app.mount('#assignment');