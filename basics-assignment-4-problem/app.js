const app = Vue.createApp({
    data(){
        return {
            first: 'user2',
            second: 'visible',
            third: '',
            flag: false
        };
    },
    methods:{
        togglePara(){
            console.log('togglePara...');
            console.log(this.flag);
            this.flag = !this.flag;
            this.second = this.flag ? 'hidden':'visible';
            // first = first + ' ' + second
            console.log('this');
            console.log(this);
            console.log('this.$data');
            console.log(this.$data);
        }
    }
});
app.mount("#assignment");